import React, { Component } from 'react';
import { Route, Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';    
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { CategoryComponent } from './components/CategoryComponent';
import { ProductComponent } from './components/ProductComponent';

const history = createBrowserHistory();

export default class App extends Component {
  displayName = App.name

  render() {
      return (
          <Router history={history} >
             <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/counter' component={Counter} />
                <Route path='/fetchdata' component={FetchData} />
                  <Route path='/categories' component={CategoryComponent} />
                  <Route path='/products' component={ProductComponent} />
             </Layout>
          </Router>
    );
  }
}
