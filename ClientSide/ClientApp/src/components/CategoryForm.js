﻿import React, { Component } from 'react';
import { CreateGUID } from './Extensions';
import { Glyphicon } from 'react-bootstrap';

export class CategoryForm extends Component {

    constructor(props) {
        super(props);

        if (this.props.choosen !== undefined && this.props.choosen.length > 0) {
            this.state = { categories: [], mode: false, loading: true };
        }
        else
            this.state = { categories: [], mode: true, loading: false };

        this.handlerToList = this.handlerToList.bind(this);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.handlerAddRow = this.handlerAddRow.bind(this);
        this.handlerChange = this.handlerChange.bind(this);
    }

    async loadingCategory(id) {
        let result;
        await fetch('api/Category/GetById/' + id)
            .then(response => response.json())
            .then(data => result = data);
        return result;
    }

    async loadingCategories() {
        let result = [];

        for (var i = 0; i < this.props.choosen.length; i++) {
            await this.loadingCategory(this.props.choosen[i])
                .then(res => result.push(res));
        }
        console.log(result);
        return result;
    }

    componentWillMount() {
        console.log('mount');
        if (this.state.loading && this.state.mode === false) {
            this.loadingCategories().then(res => this.setState({ categories: res, loading: false }));
            console.log(this.state.categories);
        }
    }
    
    handlerSubmit(event) {
        event.preventDefault();
        let xhr = new XMLHttpRequest();
        let data = JSON.stringify(this.state.categories);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (this.state.mode)
                    this.setState({ categories: [], mode: true });
                else
                    this.loadingCategories();
            }
        };

        if (this.state.mode)
            xhr.open("POST", "api/category/create", true);
        else
            xhr.open("POST", "api/category/edit", true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(data);
    }

    handlerAddRow(event) {
        event.preventDefault();
        let list = this.state.categories;
        let guid = CreateGUID();
        list.push({ id: guid, name: '' });
        this.setState({ categories: list });
    }

    handlerChange(event) {
        event.preventDefault();

        let list = this.state.categories;

        for (var i = 0; i < list.length; i++) {
            if (list[i].id === event.target.id) {
                list[i].name = event.target.value;
                break;
            }
        }

        this.setState({ categories: list });
    }

    handlerToList(event) {
        this.props.onDisplayList();
    }

    renderCategoryCreateForm() {
        return (
            <form onSubmit={this.handlerSubmit} >
                <h2>Form create</h2>
                <button onClick={this.handlerToList}>
                    <Glyphicon glyph='chevron-left' /> To list
                </button>
                <button onClick={this.handlerAddRow}>Add</button>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.categories.map(category =>
                            (<tr key={category.id} >
                                <td>
                                    <input id={category.id} type='text' required maxLength='100' value={category.name} onChange={this.handlerChange} />
                                </td>
                            </tr>)
                            )}
                    </tbody>
                </table>
                <button type="submit">Save</button>
            </form>
            );
    }

    renderCategoryEditForm() {
        return (
            <form onSubmit={this.handlerSubmit}>
                <h2>Form edit</h2>
                <button onClick={this.handlerToList}>
                    <Glyphicon glyph='chevron-left' /> To list
                </button>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.categories.map(category =>
                            (<tr key={category.id}>
                                <td>
                                    <input type='text' value={category.id} readOnly />
                                </td>
                                <td>
                                    <input id={category.id} type='text' required maxLength='100' value={category.name} onChange={this.handlerChange} />
                                </td>
                            </tr>)
                        )}
                    </tbody>
                </table>
                <button type='submit'>Save</button>
            </form>
            );
    }

    render() {
        let content = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.state.mode
                ? this.renderCategoryCreateForm()
                : this.renderCategoryEditForm();
            


        return (
            <div>
                {content}
            </div>
        );

    }
}