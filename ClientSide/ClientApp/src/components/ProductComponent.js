﻿import React, { Component } from 'react';
import { ProductsList } from './ProductsList';
import { ProductForm } from './ProductForm';

export class ProductComponent extends Component {

    constructor(props) {
        super(props);

        this.state = { displayList: true, choosen: [] };

        this.onDisplayForm = this.onDisplayForm.bind(this);
        this.onDisplayList = this.onDisplayList.bind(this);
    }

    onDisplayForm(ids) {
        this.setState({ displayList: false, choosen: ids });
    }

    onDisplayList(event) {
        this.setState({ displayList: true });
    }

    static async loadingProducts() {
        let result = [];

        await fetch('api/Product/GetEntities')
            .then(response => response.json())
            .then(data => result = data);

        return result;
    }

    render() {
        let content = this.state.displayList
            ? (<ProductsList onDisplayForm={this.onDisplayForm} />)
            : (<ProductForm onDisplayList={this.onDisplayList} choosen={this.state.choosen} />);

        return (
            <div>
                <h2>Products</h2>
                {content}
            </div>
        );
    }
}