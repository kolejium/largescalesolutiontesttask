﻿import React, { Component } from 'react';
import { CategoryForm } from './CategoryForm';
import { CategoriesList } from './CategoriesList';


export class CategoryComponent extends Component {

    constructor(props) {
        super(props);

        this.state = { displayList: true, choosen: [] };

        this.onDisplayForm = this.onDisplayForm.bind(this);
        this.onDisplayList = this.onDisplayList.bind(this);
    }

    static async loadingCategories() {
        let result = [];

        await fetch('api/Category/GetEntities')
            .then(response => response.json())
            .then(data => result = data);

        return result;
    }

    onDisplayForm(ids) {
        this.setState({ displayList: false, choosen: ids });
    }

    onDisplayList(event) {
        this.setState({ displayList: true });
    }

    render() {
        let content = this.state.displayList
            ? (<CategoriesList onDisplayForm={this.onDisplayForm} />)
            : (<CategoryForm onDisplayList={this.onDisplayList} choosen={this.state.choosen} />);

        return (
            <div>
                <h2>Categories</h2>
                {content}
            </div>
            );
    }
}