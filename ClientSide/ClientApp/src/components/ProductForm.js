﻿import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';
import { CategoryComponent } from './CategoryComponent';
import { CreateGUID } from './Extensions';
import { Glyphicon } from 'react-bootstrap';

export class ProductForm extends Component {

    constructor(props) {
        super(props);       

        CategoryComponent.loadingCategories().then(x => this.globalCategories = x);

        if (this.props.choosen !== undefined && this.props.choosen.length > 0) {
            this.state = { products: [], categories: [], mode: false, loading: true, error: false };
        }
        else
            this.state = { products: [], categories: [], mode: true, loading: false, error: false };

        this.handlerToList = this.handlerToList.bind(this);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.handlerAddRow = this.handlerAddRow.bind(this);
        this.handlerChange = this.handlerChange.bind(this);
    }

    componentWillMount() {
        console.log('mount');         

        if (this.state.loading && this.state.mode === false) {
            this.loadingProducts().then(res => {
                let mas = [];
                for (var i = 0; i < res.length; i++) {
                    let el = { id: res[i].id, name: '', value: res[i].categoryId };

                    for (var j = 0; j < this.globalCategories.length; j++) {
                        if (this.globalCategories[j].id === el.value) {
                            el.name = this.globalCategories[j].name;
                            break;
                        }
                    }

                    mas.push(el);
                }
                this.setState({ products: res, categories: mas, loading: false });
            });
        }
    }

    async loadingProduct(id) {
        let result;
        await fetch('api/Product/GetById/' + id)
            .then(response => response.json())
            .then(data => result = data);
        return result;
    }

    async loadingProducts() {
        let result = [];

        for (var i = 0; i < this.props.choosen.length; i++) {
            await this.loadingProduct(this.props.choosen[i])
                .then(res => result.push(res));
        }
        console.log(result);
        return result;
    }

    handlerSubmit(event) {
        event.preventDefault();
        let xhr = new XMLHttpRequest();

        let prod = this.state.products;

        for (let i = 0; i < this.state.categories.length; i++) {
            for (let j = 0; j < prod.length; j++) {
                if (prod[j].id === this.state.categories[i].id) {
                    prod[j].categoryId = this.state.categories[i].value;
                    break;
                }
            }
        }

        this.setState({ products: prod });

        let data = JSON.stringify(this.state.products);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (this.state.mode)
                    this.setState({ products: [], mode: true });
                else
                    this.loadingProducts();
            }
        };

        if (this.state.mode)
            xhr.open("POST", "api/product/create", true);
        else
            xhr.open("POST", "api/product/edit", true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(data);
    }

    handlerAddRow(event) {
        event.preventDefault();
        let list = this.state.products;
        let guid = CreateGUID();
        list.push({ id: guid, name: '', price: 0, count: 0, description: '', categoryId: '' });
        let mas = this.state.categories;
        mas.push({ id: guid, name: '', value: '' });
        this.setState({ products: list, categories: mas });
    }

    handlerChange(event) {
        event.preventDefault();

        let list = this.state.products;
        for (var i = 0; i < list.length; i++) {
            if (list[i].id === event.target.id) {
                let x = list[i];
                x[event.target.name] = event.target.value;
                list[i] = x;
                break;
            }
        }

        this.setState({ products: list });
    }

    getValueById(id) {
        for (var i = 0; i < this.state.categories.length; i++) {
            if (this.state.categories[i].id === id)
                return this.state.categories[i].name;
        }
    }

    onChangeCategory(id, value) {
        let mas = this.state.categories;
    }

    setValueById(id, value) {
        let mas = this.state.categories;

        for (var i = 0; i < mas.length; i++) {
            if (mas[i].id === id) {
                mas[i].name = value;
                break;
            }
        }

        this.setState({ categories: mas });
    }

    handlerToList(event) {
        this.props.onDisplayList();
    }

    renderProductCreateForm() {
        return (
            <form onSubmit={this.handlerSubmit}>
                <h2>Form create</h2>
                <button onClick={this.handlerToList}>
                    <Glyphicon glyph='chevron-left' /> To list 
                </button>
                <button onClick={this.handlerAddRow}>
                    Add
                </button>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Count</th>
                            <th>Description</th>
                            <th>Category Id</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.products.map(product =>
                            (<tr key={product.id}>
                                <td><input id={product.id} name='name' required type='text' value={product.name} onChange={this.handlerChange} /></td>
                                <td><input id={product.id} name='price' required type='number' value={product.price} onChange={this.handlerChange}/></td>
                                <td><input id={product.id} name='count' required type='number' value={product.count} onChange={this.handlerChange} /></td>
                                <td><textarea id={product.id} name='description' onChange={this.handlerChange} value={product.description}/></td>
                                <td>
                                    <Autocomplete items={this.globalCategories}
                                        getItemValue={item => item.name}
                                        inputProps={
                                            {
                                                required: true,
                                                className: this.state.error ? 'Error' : '',
                                                onBlur: () => {
                                                    let mas = this.state.categories;
                                                    let category;
                                                    let index;

                                                    console.log(this.globalCategories);
                                                    this.setState({ error: false });

                                                    for (var i = 0; i < mas.length; i++) {
                                                        if (product.id === mas[i].id) {
                                                            category = this.state.categories[i];
                                                            index = i;
                                                            break;
                                                        }
                                                    }

                                                    if (category === undefined)
                                                        return;

                                                    if (category.value !== undefined && category.value !== '') {
                                                        for (let j = 0; j < this.globalCategories.length; j++) {
                                                            if (this.globalCategories[j].id === category.id && this.globalCategories[j].name === category.name) {
                                                                return;
                                                            }
                                                        }                                           
                                                    } 
                                                    
                                                    let find = false;
                                                    if (category.name !== undefined && category.name !== '')
                                                        for (let j = 0; j < this.globalCategories.length; j++) {
                                                            if (this.globalCategories[j].name.indexOf(category.name) !== -1 || category.name.indexOf(this.globalCategories[j].name) !== -1) {
                                                                category.value = this.globalCategories[j].id;
                                                                category.name = this.globalCategories[j].name;
                                                                find = true;
                                                                break;
                                                            }
                                                        }
                                                    if (!find) {
                                                        category.value = undefined;
                                                        category.name = undefined;
                                                        this.setState({ error: true });
                                                    }
                                                    else
                                                        this.setState({ error: false });

                                                    mas[index] = category;
                                                    this.setState({ categories: mas });
                                                }
                                            }

                                        }
                                        sortItems={(a, b, value) => {
                                            let aLower = a.name.toLowerCase();
                                            let bLower = b.name.toLowerCase();
                                            let valueLower = value.toLowerCase();
                                            let queryPosA = aLower.indexOf(valueLower);
                                            let queryPosB = bLower.indexOf(valueLower);
                                            if (queryPosA !== queryPosB) {
                                                return queryPosB - queryPosA;
                                            }
                                            let acount = 0;
                                            let bcount = 0;
                                            for (var i = 0; i < value.length; i++) {
                                                if (aLower.indexOf(value[i]) !== -1)
                                                    acount++;
                                                if (bLower.indexOf(value[i]) !== -1)
                                                    bcount++;
                                            }
                                            if (acount !== bcount)
                                                return acount > bcount ? 1 : -1;
                                            return aLower < bLower ? -1 : 1;
                                        }
                                        }
                                        renderItem={(item, highlighted) =>
                                            (<div key={item.id} style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}>
                                                {item.name}
                                            </div>)
                                        }
                                        value={this.getValueById(product.id)}
                                        onChange={e => this.setValueById(product.id, e.target.value)}
                                        onSelect={(value, item) => {
                                            let mas = this.state.categories;

                                            for (var i = 0; i < mas.length; i++) {
                                                if (mas[i].id === product.id) {
                                                    mas[i] = { id: mas[i].id, name: item.name, value: item.id };
                                                    break;
                                                }
                                            }

                                            this.setState({ categories: mas });
                                        }}
                            />
                                </td>
                            </tr>))}
                    </tbody>
                </table>
                <button type="submit"> Save</button>
            </form>
            );
    }

    renderProductEditForm() {
        return (
            <form onSubmit={this.handlerSubmit}>
                <h2>Form edit</h2>
                <button onClick={this.handlerToList}>
                    <Glyphicon glyph='chevron-left' /> To list
                </button>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Count</th>
                            <th>Description</th>
                            <th>Category Id</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.products.map(product =>
                            (<tr key={product.id}>
                                <td><input id={product.id} name='name' type='text' value={product.name} onChange={this.handlerChange} /></td>
                                <td><input id={product.id} name='price' type='number' value={product.price} onChange={this.handlerChange} /></td>
                                <td><input id={product.id} name='count' type='number' value={product.count} onChange={this.handlerChange} /></td>
                                <td><textarea id={product.id} name='description' onChange={this.handlerChange} value={product.description} /></td>
                                <td>
                                    <Autocomplete items={this.globalCategories}
                                        getItemValue={item => item.name}
                                        inputProps={
                                            {
                                                reqiured: true,
                                                className: () => { return this.state.error ? 'Error' : ''; },
                                                onBlur: () => {
                                                    let mas = this.state.categories;
                                                    let category;
                                                    let index;

                                                    console.log(this.globalCategories);
                                                    this.setState({ error: false });

                                                    for (var i = 0; i < mas.length; i++) {
                                                        if (product.id === mas[i].id) {
                                                            category = this.state.categories[i];
                                                            index = i;
                                                            break;
                                                        }
                                                    }

                                                    if (category === undefined)
                                                        return;

                                                    if (category.value !== undefined && category.value !== '') {
                                                        for (let j = 0; j < this.globalCategories.length; j++) {
                                                            if (this.globalCategories[j].id === category.id && this.globalCategories[j].name === category.name) {
                                                                return;
                                                            }
                                                        }
                                                    }

                                                    let find = false;
                                                    if (category.name !== undefined && category.name !== '')
                                                        for (let j = 0; j < this.globalCategories.length; j++) {
                                                            if (this.globalCategories[j].name.indexOf(category.name) !== -1 || category.name.indexOf(this.globalCategories[j].name) !== -1) {
                                                                category.value = this.globalCategories[j].id;
                                                                category.name = this.globalCategories[j].name;
                                                                find = true;
                                                                break;
                                                            }
                                                        }
                                                    if (!find) {
                                                        category.value = undefined;
                                                        category.name = undefined;
                                                        this.setState({ error: true });
                                                    }
                                                    else
                                                        this.setState({ error: false });

                                                    mas[index] = category;
                                                    this.setState({ categories: mas });
                                                }
                                            }
                                        }
                                        sortItems={(a, b, value) => {
                                            let aLower = a.name.toLowerCase();
                                            let bLower = b.name.toLowerCase();
                                            let valueLower = value.toLowerCase();
                                            let queryPosA = aLower.indexOf(valueLower);
                                            let queryPosB = bLower.indexOf(valueLower);
                                            if (queryPosA !== queryPosB) {
                                                return queryPosB - queryPosA;
                                            }
                                            let acount = 0;
                                            let bcount = 0;
                                            for (var i = 0; i < value.length; i++) {
                                                if (aLower.indexOf(value[i]) !== -1)
                                                    acount++;
                                                if (bLower.indexOf(value[i]) !== -1)
                                                    bcount++;
                                            }
                                            if (acount !== bcount)
                                                return acount > bcount ? 1 : -1;
                                            return aLower < bLower ? -1 : 1;
                                        }
                                        }
                                        renderItem={(item, highlighted) =>
                                            (<div key={item.id} style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}>
                                                {item.name}
                                            </div>)
                                        }
                                        value={this.getValueById(product.id)}
                                        onChange={e => this.setValueById(product.id, e.target.value)}
                                        onSelect={(value, item) => {
                                            let mas = this.state.categories;

                                            for (var i = 0; i < mas.length; i++) {
                                                if (mas[i].id === product.id) {
                                                    mas[i] = { id: mas[i].id, name: item.name, value: item.id };
                                                    break;
                                                }
                                            }

                                            this.setState({ categories: mas });
                                        }}
                                    />
                                </td>
                            </tr>))}
                    </tbody>
                </table>
                <button type="submit">Save</button>
            </form>
        );
    }

    render() {
        let content = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.state.mode
                ? this.renderProductCreateForm()
                : this.renderProductEditForm();
        return content;
    }
}