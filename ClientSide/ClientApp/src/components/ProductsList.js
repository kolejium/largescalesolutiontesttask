﻿import React, { Component } from 'react';
import { ProductForm } from './ProductForm';
import { ProductComponent } from './ProductComponent';

export class ProductsList extends Component {

    constructor(props) {
        super(props);
        this.state = { products: [], searchProducts: [], loading: true, choosen: [] };

        this.loadingProducts();

        this.handlerSearch = this.handlerSearch.bind(this);
        this.handlerCheckBoxChange = this.handlerCheckBoxChange.bind(this);
        this.handlerOnClickRecord = this.handlerOnClickRecord.bind(this);
        this.handlerOnCreate = this.handlerOnCreate.bind(this);
        this.handlerOnDelete = this.handlerOnDelete.bind(this);
        this.handlerOnUpdate = this.handlerOnUpdate.bind(this);
        this.handlerExport = this.handlerExport.bind(this);
        this.handlerLoadFile = this.handlerLoadFile.bind(this);
        this.handlerOnOpenFileDialog = this.handlerOnOpenFileDialog.bind(this);

    }

    loadingProducts() {
        ProductComponent.loadingProducts().then(x => this.setState({ products: x, searchProducts: x, loading: false, choosen: [] }));
    }

    handlerCheckBoxChange(event) {
        let data = [];
        if (this.state.choosen.indexOf(event.target.value) === -1) {
            data = this.state.choosen;
            data.push(event.target.value);
        }
        else {
            for (let i = 0; i < this.state.choosen.length; i++) {
                if (this.state.choosen[i] !== event.target.value)
                    data.push(this.state.choosen[i]);
            }
        }
        this.setState({ choosen: data });
    }

    handlerOnDelete(event) {
        event.preventDefault();
        let data = [];

        for (let i = 0; i < this.state.choosen.length; i++) {
            data.push(this.state.products[this.state.products.findIndex(x => x.id === this.state.choosen[i])]);
        }

        if (data === null || data.length === 0)
            return;

        let xhr = new XMLHttpRequest();
        xhr.open("POST", "api/product/delete", true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4)
                this.loadingProducts();
        };
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(data));
    }

    handlerOnUpdate(event) {
        this.props.onDisplayForm(this.state.choosen);
    }

    handlerOnClickRecord(event) {
        let data = event.target.parentElement.getAttribute('data-item-id');
        if (event.target.type !== 'checkbox')
            this.props.onDisplayForm(new Array(data));
    }

    handlerOnCreate(event) {
        this.props.onDisplayForm([]);
    }

    handlerExport(event) {
        let link = document.createElement('a');
        link.href = 'api/product/export';
        link.download = 'true';
        document.body.appendChild(link);
        link.click(); // hack not good...
        document.body.removeChild(link);
    }

    handlerOnOpenFileDialog(event) {
        this.refs.inputOpenFileRef.click();
    }

    handlerLoadFile(event) {
        let file = event.target.files[0];

        let reader = new FileReader();

        reader.onload = (e) => {
            let lines = e.target.result.split("\r\n");

            let names = lines[0].split(";");

            let data = [];
            for (let i = 1; i < lines.length; i++) {
                let line = lines[i].split(";");

                let next = true;

                for (let j = 0; j < line.length; j++) {
                    if (line[j] !== "")
                        next = false;
                }

                if (next)
                    continue;

                let el = {};

                for (let p = 0; p < names.length; p++) {
                    el[names[p]] = line[p];
                }
                data.push(el);
            }

            data = JSON.stringify(data);

            let xhr = new XMLHttpRequest();
            xhr.open("POST", "api/product/import", true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4)
                    this.loadingProducts();
            };
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(data);
            console.log(e.target.result);
        };
        reader.readAsText(file);
    }

    handlerSearch(event) {
        event.preventDefault();
        let prods = this.state.products;
        let items = prods.filter((item) => {
            return item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
        });
        this.setState({ searchProducts: items });
    }

    renderProductTable() {
        return (
            <div>
                <div>
                    <button className='Enable' onClick={this.handlerOnCreate}>
                        Add
                    </button>
                    <button type='button' className={this.state.choosen.length > 0 ? 'Enable' : 'Disable'} disabled={this.state.choosen.length <= 0} onClick = {this.handlerOnDelete} >
                        Delete
                    </button>
                    <button className={this.state.choosen.length > 0 ? 'Enable' : 'Disable'} disabled={this.state.choosen.length <= 0} onClick={this.handlerOnUpdate}>
                        Edit
                    </button>
                    <button className={this.state.products.length > 0 ? 'Enable' : 'Disable'} onClick={this.handlerExport} >
                        Export
                    </button>
                    <button className='Enable' onClick={this.handlerOnOpenFileDialog}>
                        Import
                    </button>
                    <input className='Invisible' ref='inputOpenFileRef' onChange={this.handlerLoadFile} type='file' />
                    <input type='text' onChange={this.handlerSearch}/>
                </div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th/>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Count</th>
                            <th>Description</th>
                            <th>Category Id</th>
                        </tr>
                    </thead>
                    <tbody onClick={this.handlerOnClickRecord}>
                        {this.state.searchProducts.map(product =>
                            (<tr key={product.id} data-item-id={product.id}>
                                <td><input type='checkbox' value={product.id} onChange={this.handlerCheckBoxChange} /></td>
                                <td>{product.id}</td>
                                <td>{product.name}</td>
                                <td>{product.price}</td>
                                <td>{product.count}</td>
                                <td>{product.description}</td>
                                <td>{product.categoryId}</td>
                            </tr>))}
                    </tbody>
                </table>
            </div>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderProductTable(this.state.products);

        return (
            <div>
                <h2>List</h2>
                {contents}
            </div>
        );
    }
}