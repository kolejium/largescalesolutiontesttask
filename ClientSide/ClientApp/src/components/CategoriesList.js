﻿import React, { Component } from 'react';

export class CategoriesList extends Component {

    constructor(props) {
        super(props);
        this.state = { categories: [], loading: true, choosen: [] };
        this.loadingCategories();

        this.handlerExport = this.handlerExport.bind(this);
        this.handlerLoadFile = this.handlerLoadFile.bind(this);
        this.handlerOnCreate = this.handlerOnCreate.bind(this);
        this.handlerOnDelete = this.handlerOnDelete.bind(this);
        this.handlerOnUpdate = this.handlerOnUpdate.bind(this);
        this.handlerCheckBoxChange = this.handlerCheckBoxChange.bind(this);
        this.handlerOnClickRecord = this.handlerOnClickRecord.bind(this);
        this.handlerOnOpenFileDialog = this.handlerOnOpenFileDialog.bind(this);
    }

    loadingCategories() {
        fetch('api/Category/GetEntities')
            .then(response => response.json())
            .then(data => this.setState({ categories: data, loading: false, choosen: [] }));
    }

    handlerCheckBoxChange(event) {
        let data = [];
        if (this.state.choosen.indexOf(event.target.value) === -1) {
            data = this.state.choosen;
            data.push(event.target.value);
        }
        else {
            for (let i = 0; i < this.state.choosen.length; i++) {
                if (this.state.choosen[i] !== event.target.value)
                    data.push(this.state.choosen[i]);
            }
        }
        this.setState({ choosen: data });
    }

    handlerOnDelete(event) {
        event.preventDefault();
        let data = [];

        for (let i = 0; i < this.state.choosen.length; i++)
            for (let j = 0; j < this.state.categories.length; j++)
                if (this.state.categories[j].id === this.state.choosen[i])
                    data.push(this.state.categories[j]);

        if (data === null || data.length === 0)
            return;

        let xhr = new XMLHttpRequest();
        xhr.open("POST", "api/category/delete", true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4)
                this.loadingCategories();
        };
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(data));
        
    }

    handlerOnUpdate(event) {
        this.props.onDisplayForm(this.state.choosen);
    }

    handlerOnCreate(event) {
        this.props.onDisplayForm([]);
    }

    handlerOnClickRecord(event) {
        let data = event.target.parentElement.getAttribute('data-item-id');
        if (event.target.type !== 'checkbox')
            this.props.onDisplayForm(new Array(data));
    }

    handlerExport(event) {
        let link = document.createElement('a');
        link.href = 'api/category/export';
        link.download = 'true';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    handlerOnOpenFileDialog(event) {
        this.refs.inputOpenFileRef.click();
    }

    handlerLoadFile(event) {
        let file = event.target.files[0];

        let reader = new FileReader();

        reader.onload = (e) => {
            let lines = e.target.result.split("\r\n");

            let names = lines[0].split(";");

            let data = [];
            for (let i = 1; i < lines.length; i++) {
                let line = lines[i].split(";");

                let next = true;

                for (let j = 0; j < line.length; j++) {
                    if (line[j] !== "")
                        next = false;
                }

                if (next)
                    continue;

                let el = {};

                for (let p = 0; p < names.length; p++) {
                    el[names[p]] = line[p];
                }
                data.push(el);
            }

            data = JSON.stringify(data);

            let xhr = new XMLHttpRequest();
            xhr.open("POST", "api/category/import", true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4)
                    this.loadingCategories();
            };
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(data);
            console.log(e.target.result);
        };
        reader.readAsText(file);
    }

    renderCategoriesTable() {
        return (
            <div>
                <div>
                    <button className='Enable' onClick={this.handlerOnCreate}>
                        Add
                    </button>
                    <button className={this.state.choosen.length > 0 ? 'Enable' : 'Disable'} onClick={this.handlerOnDelete}>
                        Delete
                    </button>
                    <button className={this.state.choosen.length > 0 ? 'Enable' : 'Disable'} onClick={this.handlerOnUpdate}>
                        Edit
                    </button>
                    <button className={this.state.categories.length > 0 ? 'Enable' : 'Disable'} onClick={this.handlerExport} >
                        Export
                    </button>
                    <button className='Enable' onClick={this.handlerOnOpenFileDialog}>
                        Import
                    </button>
                    <input className='Invisible' ref='inputOpenFileRef' onChange={this.handlerLoadFile} type='file' />
                </div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody onClick={this.handlerOnClickRecord}>
                        {this.state.categories.map(category =>
                            (<tr key={category.id} data-item-id={category.id} >
                                <td><input type='checkbox' value={category.id} onChange={this.handlerCheckBoxChange} /></td>
                                <td>{category.id}</td>
                                <td>{category.name}</td>
                            </tr>))}
                    </tbody>
                </table>
            </div>
            );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCategoriesTable();

        return (
            <div>
                <h1>List</h1>
                {contents}
            </div>
        );
    }
}