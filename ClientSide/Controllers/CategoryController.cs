﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServerSide;
using ServerSide.Entities;
using ServerSide.Repositories;
using Microsoft.Extensions.Configuration;
using PetaPoco;
using System;
using System.Linq;
using System.IO;
using System.Reflection;

namespace ClientSide.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly IConfiguration _configuration;

        private UnitOfWork _unitOfWork;
        private Repository<CategoryEntity> _repository;

        public CategoryController(IConfiguration configuration)
        {
            _configuration = configuration;

            var db = DatabaseConfiguration
                .Build()
                .UsingConnectionString(configuration.GetConnectionString("postgres"))
                .UsingProviderName("Npgsql")
                .Create();
            _unitOfWork = new UnitOfWork(db);
            _repository = _unitOfWork.Repository<CategoryEntity>();
        }

        [HttpGet("[action]/{id}")]
        public CategoryEntity GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        [HttpPost("[action]")]
        public void Create([FromBody]CategoryEntity[] categories)
        {
            CreateHelper(categories);
        }

        private void CreateHelper(CategoryEntity[] categories, bool hasId = false)
        {
            foreach (var item in categories)
            {
                if (!hasId)
                    item.Id = Guid.NewGuid();
                _repository.Insert(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpPost("[action]")]
        public void Edit([FromBody]CategoryEntity[] categories)
        {
            foreach (var item in categories)
            {
                _repository.Update(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpPost("[action]")]
        public void Delete([FromBody]CategoryEntity[] categories)
        {
            foreach (var item in categories)
            {
                _repository.Delete(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpGet("[action]")]
        [Produces("text/csv")]
        public IActionResult Export()
        {
            var ms = new MemoryStream();

            var sw = new StreamWriter(ms);

            var properties = typeof(CategoryEntity).GetProperties().Where(x => x.GetMethod.IsVirtual == false).ToArray();

            var line = "";

            for (int i = 0; i < properties.Length; i++)
            {
                line += i != properties.Count() - 1 ? properties[i].Name + ";" : properties[i].Name;
            }

            sw.WriteLine(line);

            foreach (var item in GetEntities())
            {
                line = "";               

                for (int i = 0; i < properties.Length; i++)
                {
                    line += i != properties.Count() - 1 ? properties[i].GetValue(item) + ";" : properties[i].GetValue(item);
                } 

                sw.WriteLine(line);
                sw.Flush();
            }

            ms.Seek(0, SeekOrigin.Begin);

            return File(ms, "text/csv", "categories.csv");
        }

        [HttpPost("[action]")]
        public void Import([FromBody]CategoryEntity[] categories)
        {
            var dbEntities = GetEntities().ToList();
            categories = categories.Where(x => dbEntities.Find(y => y.Id == x.Id) == null && x.Id != Guid.Empty).ToArray();
            CreateHelper(categories, true);
        }

        [HttpGet("[action]")]
        public IEnumerable<CategoryEntity> GetEntities()
        {
            return _repository.GetEntities();
        }
    }
}