﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PetaPoco;
using ServerSide;
using ServerSide.Entities;
using ServerSide.Repositories;

namespace ClientSide.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IConfiguration _configuration;

        private UnitOfWork _unitOfWork;
        private Repository<ProductEntity> _repository;

        public ProductController(IConfiguration configuration)
        {
            _configuration = configuration;

            var db = DatabaseConfiguration
                .Build()
                .UsingConnectionString(configuration.GetConnectionString("postgres"))
                .UsingProviderName("Npgsql")
                .Create();
            _unitOfWork = new UnitOfWork(db);
            _repository = _unitOfWork.Repository<ProductEntity>();
        }

        [HttpGet("[action]/{id}")]
        public ProductEntity GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        [HttpPost("[action]")]
        public void Create([FromBody]ProductEntity[] products)
        {
            CreateHelper(products);
        }

        private void CreateHelper(ProductEntity[] products, bool hasId = false)
        {
            foreach (var item in products)
            {
                if (!hasId)
                    item.Id = Guid.NewGuid();
                _repository.Insert(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpPost("[action]")]
        public void Edit([FromBody]ProductEntity[] products)
        {
            foreach (var item in products)
            {
                _repository.Update(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpPost("[action]")]
        public void Delete([FromBody]ProductEntity[] products)
        {
            foreach (var item in products)
            {
                _repository.Delete(item);
            }
            _unitOfWork.Save(_repository);
        }

        [HttpGet("[action]")]
        [Produces("text/csv")]
        public IActionResult Export()
        {
            var ms = new MemoryStream();

            var sw = new StreamWriter(ms);

            var properties = typeof(ProductEntity).GetProperties().Where(x => x.GetMethod.IsVirtual == false).ToArray();

            var line = "";

            for (int i = 0; i < properties.Length; i++)
            {
                line += i != properties.Count() - 1 ? properties[i].Name + ";" : properties[i].Name;
            }

            sw.WriteLine(line);

            foreach (var item in GetEntities())
            {
                line = "";

                for (int i = 0; i < properties.Length; i++)
                {
                    line += i != properties.Count() - 1 ? properties[i].GetValue(item) + ";" : properties[i].GetValue(item);
                }

                sw.WriteLine(line);
                sw.Flush();
            }

            ms.Seek(0, SeekOrigin.Begin);

            return File(ms, "text/csv", "products.csv");
        }

        [HttpPost("[action]")]
        public void Import([FromBody]ProductEntity[] products)
        {
            var dbEntities = GetEntities().ToList();
            products = products.Where(x => dbEntities.Find(y => y.Id == x.Id) == null && x.Id != Guid.Empty).ToArray();
            CreateHelper(products, true);
        }


        [HttpGet("[action]")]
        public IEnumerable<ProductEntity> GetEntities()
        {
            var x = _repository.GetEntities();
            return x;
        }

    }
}