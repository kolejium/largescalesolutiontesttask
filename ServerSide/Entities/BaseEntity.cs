﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerSide.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
