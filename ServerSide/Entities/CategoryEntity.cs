﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServerSide.Entities
{
    [TableName("CategoryTable")]
    public class CategoryEntity : BaseEntity
    {
        public string Name { get; set; }

        [Ignore]
        public virtual IList<ProductEntity> Products { get; set; }
    }
}
