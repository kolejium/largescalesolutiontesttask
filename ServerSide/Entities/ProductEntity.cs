﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServerSide.Entities
{
    [TableName("ProductTable")]
    public class ProductEntity : BaseEntity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public int Count { get; set; }
        public string Description { get; set; }

        public Guid CategoryId { get; set; }
    }
}
