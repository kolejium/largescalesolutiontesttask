﻿using ServerSide.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServerSide.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Delete(T entity);
        void Insert(T entity);
        void Update(T entity);

        T GetById(object id);
        IEnumerable<T> GetEntities();
    }
}
