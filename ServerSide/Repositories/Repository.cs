﻿using PetaPoco;
using ServerSide.Entities;
using ServerSide.Interfaces;
using System;
using System.Collections.Generic;

namespace ServerSide.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private Database _context;
        private IList<Action> _operations;

        public IList<Action> Operations => _operations ?? (_operations = new List<Action>());

        public Repository(Database context)
        {
            _context = context;
        }

        public void Delete(T entity)
        {
            Operations.Add(() => _context.Delete(entity));
        }

        public T GetById(object id)
        {
            return _context.Single<T>(id);
        }

        public IEnumerable<T> GetEntities()
        {
            return _context.Fetch<T>();
        }

        public void Insert(T entity)
        {
            Operations.Add(() => {
                _context.Insert(entity);
            });
        }

        public void Update(T entity)
        {
            Operations.Add(() => _context.Update(entity));
        }
    }
}
