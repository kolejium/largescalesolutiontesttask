﻿using PetaPoco;
using ServerSide.Entities;
using ServerSide.Repositories;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ServerSide
{
    public class UnitOfWork : IDisposable
    {
        private IDatabase _context;

        public UnitOfWork(IDatabase context)
        {
            _context = context;
        }

        public void Save<T>(Repository<T> repository) where T : BaseEntity
        {
            if (repository.Operations.Count == 0)
                return;

            try
            {
                _context.BeginTransaction();
                repository.Operations.ToList().ForEach(x => x.Invoke());
                _context.CompleteTransaction();
            }
            catch(Exception e)
            {
                _context.AbortTransaction();
            }
        }

        public Repository<T> Repository<T>() where T : BaseEntity
        {
            return (Repository<T>)Activator.CreateInstance(typeof(Repository<>).MakeGenericType(typeof(T)), _context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
